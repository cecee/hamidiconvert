import sys, getopt
import json
import glob
from shutil import copyfile
import os
import numpy as np
from shutil import copyfile
import re	#정규식 라이브러리

from data_base.postgreSql import *
from data_base.sqlite import *
from data_base.excell import *

def main(argv):
 
    ###############################################################################    
    postSql = PostSQL() 
    ha_sqlite = HaSqlite() 

    db_jdoc={"genre": 33, "title": "신곡테스트", "singer": "", "songno": "10", "writer": "", "keycode": "Ab", "codepage": 1200, "composer": "", "keycoden": 66, "songdate": "2006-03-02T00:00:00", "subtitle": "", "keygender": "a", "countrycode": 410}
    fetch_data = ha_sqlite.fetchAll()
    for i in fetch_data:
        #print(i)
        db_jdoc["songno"]=str(i[0])
        db_jdoc["genre"]=i[1]
        db_jdoc["title"]=i[2]
        db_jdoc["subtitle"]=i[3]
        keycode=i[4]
       
        items = re.findall('\(([^)]+)', keycode)
        code=items[0]
        if(keycode.find('남')>=0): sex='m'
        elif(keycode.find('여')>=0): sex='w'
        else: sex='a'
        #print(items[0])
        db_jdoc["keycode"]=code

        db_jdoc["composer"]=i[5]
        db_jdoc["writer"]=i[6]
        db_jdoc["singer"]=i[7]
        db_jdoc["keycoden"]=i[8]
         
        sDate = str(i[9]) 
        year=sDate[0]+sDate[1]+sDate[2]+sDate[3]
        month=sDate[4]+sDate[5]
        day=sDate[6]+sDate[7]
        tDate='{}-{}-{}T00:00:00'.format(year, month, day)
        #db_jdoc["songdate"]=i[9]
        db_jdoc["songdate"]=tDate

        db_jdoc["countrycode"]=i[10]
        db_jdoc["codepage"]=i[11]

        #db_jdoc["keygender"]=i[12] #m,w , a
        db_jdoc["keygender"]=sex #m,w , a
        print(db_jdoc)
        postSql.updatePostSQL(i[0], db_jdoc)
if __name__ == "__main__":
    main(sys.argv)