import sys, getopt
import json
import glob
import shutil
import time
import os
import numpy as np
import ctypes
import re	#정규식 라이브러리
import constant

from shutil import copyfile
from datetime import date
from data_base.postgreSql import *
from data_base.sqlite import *
from data_base.excell import *

def removeAllFile(filePath):
    #print(filePath)
    if os.path.exists(filePath):
        for file in os.scandir(filePath):
            os.remove(file)
        return 'ok'
    else:
        return 'err'
def mk_backup_src_folder():
    today = date.today()
    folder = "./data_backup/backup_src/{0}".format(today)
    os.makedirs(folder, exist_ok=True)
    #print(folder)
    return folder

def mk_backup_dst_folder():
    today = date.today()
    folder = "./data_backup/backup_dst/{0}".format(today)
    os.makedirs(folder, exist_ok=True)
    #print(folder)
    return folder

def backAllFile(filePath):
    folder = mk_backup_src_folder()
    if os.path.exists(filePath):
        for file in os.scandir(filePath):
            shutil.copy(file, folder)
        return 'ok'
    else:
        return 'err'
    

def main(argv):
    FILE_NAME     = argv[0] # command line arguments의 첫번째는 파일명
    OUT_FILENAME  = ""     # 채널명 초기화
    #SONG_NUMBER ="100000"
    SONG_NUMBER =""
    RELEASE="Test Sample"
    #test()

    #calcpkg.operation.add(10, 20)
    try:
        # opts: getopt 옵션에 따라 파싱 ex) [('-i', 'myinstancce1')]
        # etc_args: getopt 옵션 이외에 입력된 일반 Argument
        # argv 첫번째(index:0)는 파일명, 두번째(index:1)부터 Arguments
        opts, etc_args = getopt.getopt(argv[1:], "hnua", ["help","number="])
        #print(etc_args)
    except getopt.GetoptError: # 옵션지정이 올바르지 않은 경우
        print('error: --number=11001 (update song number) -u <update>')
        sys.exit(2)

    for opt, arg in opts: # 옵션이 파싱된 경우
        if opt in ("-h", "--help"): # HELP 요청인 경우 사용법 출력
            print('help: --number=11001 (update song number) -u <update>')
            sys.exit()
        elif opt in ("-n", "--number"): # 인스턴명 입력인 경우
            SONG_NUMBER = arg
        elif opt in ("-u"): # 인스턴명 입력인 경우
            RELEASE = "RELEASE"
        elif opt in ("-a", "--auto"): # 인스턴명 입력인 경우
            SONG_NUMBER = 'AUTO'
        # elif opt in ("-o", "--outfile"): # 채널명 입력인 경우
        #     OUT_FILENAME = arg

    # 필수항목 값이 비어있다면
    if len(SONG_NUMBER) < 1: # 필수항목 값이 비어있다면
        print("### Need song number!!") # 필수임을 출력
        #sys.exit(2)

     
    
    constant.MODE=RELEASE
    print("START SONG_NUMBER:",SONG_NUMBER)  
    print("RELEASE MODE ",constant.MODE) 

    excelFile = './data_backup/chk_list.xlsx'

    #sys.exit()
    ###############################################################################    
    excel = MkExcell()
    postSql = PostSQL() 
    w_sqlite = WSqlite() 

    if(SONG_NUMBER == 'AUTO'):
        #sn=ha_sqlite.getSongNumber() #sqlite db에 저장된 번호 뒤부터 처리
        sn=postSql.getSongNumber() #postgre db에 저장된 번호 뒤부터 처리        
    else:
        sn = int(SONG_NUMBER)
    print('=> start update Song Number:{0} '.format(sn))

    ########################### Delete temp Directory #############################
    print('=> remove all file ')
    res=removeAllFile('./data_s/data_midi_txt')
    res=removeAllFile('./data_s/data_mp3')
    res=removeAllFile('./data_s/data_w')
    w_sqlite.deleteAll()

    ############################# Backup src file #################################
    backAllFile('./data_s/data_cmmidi')
    ###############################################################################
    ha_files=glob.glob(os.path.abspath('.')+"/data_s/data_cmmidi/*.mid")
    print(ha_files)
    for src_file in ha_files:
        head, tail = os.path.split(src_file)    
        tail= tail.lower()
        #tail=tail.replace(".mid",".txt")
        #print("head:"+head+"\n")  
        #print("tail:"+tail+"\n")  
        des_file = f'{head}/{sn}.mid' #F 문자열 사용
        #print("### desfile:"+des_file) 
        excel.mk_list( 0, sn, src_file, des_file) 
        sn=sn+1
        #if os.path.isfile(des_file):
        #    des_file=
        #    print("###OK isfile : %d" %sn) 
        #    excel.mk_list( 0, sn, src_file, des_file) 
        #   sn=sn+1
        #else: 
        #    print("###NG isfile : %d" %sn) 
        #    excel.mk_list( 1, 0, src_file, "xxxx") 
        #print('{0} | {1}'.format(head, tail))
    rd_list = excel.rd_list()
    ex_list= rd_list.values
    backup_dst_folder = mk_backup_dst_folder()

    for each_list in ex_list:
        #print('####{0} | {1} | {2}\n'.format(each_list[0], each_list[1],each_list[2]))

        src = each_list[1]
        dst = "./data_s/data_midi_txt/%d.mid" % each_list[0]
        dst= dst.replace("\\","/")
        #dst = each_list[2]
        print('src-> {0} | des->{1} '.format(src, dst))
        copyfile(src, dst) #copy midi file
       
        #src= src.replace("\\","/")
        #dst = "./data_s/data_midi_txt/%d.mid" % each_list[0]
        #dst= dst.replace("\\","/")
        #print('src-> {0} | des->{1} '.format(src, dst))
        #copyfile(src, dst) #copy midi file
        ######################################################
        
        #head, tail = os.path.split(src)  
        #print("HEAD :"+ head)  
        mid_inp=".\\data_s\\data_midi_txt\\%d.mid" % each_list[0]
        mp3_out=".\\data_s\\data_mp3\\%d.mp3" % each_list[0]
        sound_font=".\\cm_midi2mp3\\soundfont.sf2"
        #mp3_out=midi_input.replace("mid","mp3")
        #midi_src= midi_src.replace("\\","/")
        print('mid_inp-> {0}\n'.format(mid_inp))
        print('mp3_out-> {0}\n'.format(mp3_out))
        midi2mp3 = (".\\cm_midi2mp3\\midiconverter.exe -i={0} -o={1} -s={2}".format(mid_inp,mp3_out,sound_font))
        print('midi2mp3-> {0}\n'.format(midi2mp3))
        os.system(midi2mp3)
        #head= head.replace("data_hamidi","data_mp3") 
        #print(head)  
        print('backup_dst_folder ->{0} '.format(backup_dst_folder))
        shutil.copy(mid_inp, backup_dst_folder)
        shutil.copy(mp3_out, backup_dst_folder)
    
    ########################################################
    #make W file
    ########################################################
        w_out = ".\\data_s\\data_w\\%d.w" % each_list[0]
        print(w_out+"\n")
        #print(dst)
        midi2w = (".\\cm_midi2w\\v2017_midi2w.exe {0} {1}".format(mid_inp, w_out))
        print(midi2w+"\n")
        os.system(midi2w)
        shutil.copy(w_out, backup_dst_folder)
        #ctypes.windll.shell32.ShellExecuteA(0, 'open', calc, None, None, 1)
   

    ########################################################
    #update mbox server
    ########################################################
    #db_jdoc={"genre": 66, "title": "신곡테스트", "singer": "", "songno": "10", "writer": "", "keycode": "Ab", "codepage": 1200, "composer": "", "keycoden": 66, "songdate": "2006-03-02T00:00:00", "subtitle": "", "keygender": "a", "countrycode": 410}
    #fetch_data = w_sqlite.fetchAll()

    db_jdoc={"genre": 33, "title": "신곡테스트", "singer": "", "songno": "10", "writer": "", "keycode": "Ab", "codepage": 1200, "composer": "", "keycoden": 66, "songdate": "2006-03-02T00:00:00", "subtitle": "", "keygender": "a", "countrycode": 410}
    fetch_data = w_sqlite.fetchAll()
    print("FETCH DATA:\n {0} \n".format(fetch_data) )
    #sys.exit()

    for i in fetch_data:
        #print(i)
        db_jdoc["songno"]=str(i[0])
        db_jdoc["genre"]=i[1]
        _title= i[2].replace("'","''")
        db_jdoc["title"]=_title
        #db_jdoc["title"]=i[2]
        _subtitle= i[3].replace("'","''")
        db_jdoc["subtitle"]=_subtitle
        #db_jdoc["subtitle"]=i[3]
        keycode=i[4]
        items = re.findall('\(([^)]+)', keycode)
        if(keycode.find('남')>=0): sex='m'
        elif(keycode.find('여')>=0): sex='w'
        else: sex='a'
        
        if(len(items)>0): _code=items[0]
        else: _code="C"
        #code=items[0]
        print(_code)
        db_jdoc["keycode"]=_code

        db_jdoc["composer"]=i[5].replace("'","''")
        db_jdoc["writer"]=i[6].replace("'","''")
        db_jdoc["singer"]=i[7].replace("'","''")
        db_jdoc["keycoden"]=i[8]
         
        sDate = str(i[9]) 
        year=sDate[0]+sDate[1]+sDate[2]+sDate[3]
        month=sDate[4]+sDate[5]
        day=sDate[6]+sDate[7]
        tDate='{}-{}-{}T00:00:00'.format(year, month, day)
        db_jdoc["songdate"]=tDate
        db_jdoc["countrycode"]=i[10]
        db_jdoc["codepage"]=i[11]
        db_jdoc["keygender"]=sex #m,w , a
        postSql.updatePostSQL(i[0], db_jdoc)
       
if __name__ == "__main__":
    main(sys.argv)
    print("\n",constant.MODE)