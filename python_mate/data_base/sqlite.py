import sqlite3
import json

print(sqlite3.version)
print(sqlite3.sqlite_version)
class WSqlite():
    def __init__(self):
        self.db = sqlite3.connect("upSqlite.db", isolation_level=None)
        self.cursor = self.db.cursor()
        #print(self.cursor)

    # def __del__(self):
    #     self.db.close()
    #     self.cursor.close()
    def execute(self,query,args={}):
        self.cursor.execute(query,args)
        row = self.cursor.fetchall()
        return row
    def commit(self):
        self.cursor.commit()

    def deleteAll(self):
        self.cursor.execute("DELETE FROM haMIDI;")

    def fetchAll(self):
        self.cursor.execute("SELECT * FROM haMIDI;")
        return self.cursor.fetchall()

    def getSongNumber(self):
        s = 'SELECT MAX(iSongNo) FROM haMIDI'
        self.cursor.execute(s)
        result = self.cursor.fetchall()
        newlist = [data[0] for data in result]
        sn=newlist[0]
        if(sn<100000):
            sn = 100000
        else:
            sn=sn+1
        return sn

