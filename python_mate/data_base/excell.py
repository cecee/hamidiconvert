import pandas as pd
import openpyxl

class MkExcell():
    def __init__(self):
        self.excelfileName='data_backup/chk_list.xlsx'
        self.wb = openpyxl.Workbook()
        self.sheet = self.wb['Sheet']
        self.sheet1 = self.wb.create_sheet('Sheet1')
        #self.sheet.delete_cols(1, 10)
    def __del__(self):
        #self.wb.save(self.excelfileName)
        self.wb.close()

    def mk_list(self, sheet, sn, src, des):
        #print(sheet.max_row)
        if sheet==0:
            row=self.sheet.max_row+1
            #print(" max_row %s" % self.sheet.max_row)
            #print(" max_column %s" % self.sheet.max_column)
            self.sheet.cell(row = row, column = 1, value=sn)
            self.sheet.cell(row = row, column = 2, value=src)
            self.sheet.cell(row = row, column = 3, value=des) 
        else:
            row=self.sheet1.max_row+1
            self.sheet1.cell(row = row, column = 1, value=src)
            self.sheet1.cell(row = row, column = 2, value=des)   

    def rd_list(self): 
        self.wb.save(self.excelfileName)
        rd_list = pd.read_excel(self.excelfileName, sheet_name = 0)
        return rd_list

