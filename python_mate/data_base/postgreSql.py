import psycopg2 as pg2
import json
import constant

class PostSQL():
    def __init__(self):
        if(constant.MODE=='RELEASE'):
            self.db=pg2.connect(   
                database="songdb",
                user="ksubox",
                password="evendontask1",
                host="15.165.60.54",
                port="5432"
            )
        else:
            self.db=pg2.connect(   
                database="matembox",
                user="singsky899",
                password="898900",
                host="35.194.163.158",
                port="5432"
            )
        self.cursor = self.db.cursor()

    def __del__(self):
        self.db.close()
        self.cursor.close()

    def execute(self,query,args={}):
        self.cursor.execute(query,args)
        row = self.cursor.fetchall()
        return row

    def commit(self):
        self.cursor.commit()

    def getSongNumber(self):
        s = 'SELECT MAX( CAST(songno AS INT)) FROM public.loaded_songs'
        self.cursor.execute(s)
        result = self.cursor.fetchall()
        newlist = [data[0] for data in result]
        sn=newlist[0]
        if(sn<100000):
            sn = 100000
        else:
            sn=sn+1
        return sn

    def updatePostSQL(self, songno, jdoc):
        #print("{} -> jdoc{}".format(songno, json.dumps(jdoc)))
        updateFlag=False
        try:
            s = "INSERT INTO public.loaded_songs (songno, jdoc) VALUES('{}', '{}');".format(songno, json.dumps(jdoc))
            #print(s)
            print('INSERT_SongNumber:',songno)
            self.cursor.execute(s)
        except Exception as err:
            #UPDATE public.loaded_songs SET jdoc='', status='' WHERE songno='';
            #print ("update reason:", err)
            #print ("Exception TYPE:", type(err))
            updateFlag=True
        finally:
            self.db.commit()
        if updateFlag:
            s = "UPDATE public.loaded_songs SET jdoc='{}' WHERE songno='{}';".format(json.dumps(jdoc), songno)
            print('UPDATE_SongNumber:',songno)
            self.cursor.execute(s)
            self.db.commit()

