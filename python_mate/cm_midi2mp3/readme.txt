.\cm_midi2mp3\midiconverter.exe -i=.\data_s\data_midi_txt\100000.mid -o=.\data_s\data_mp3\100000.mp3 -s=.\cm_midi2mp3\soundfont.sf2

Examples:
midiconverter.exe -i=input.mid -o=output.mp3 -s=soundfont.sf2 -b=320 -r=0
midiconverter.exe -i=input.mid -o=output.wav -s=newsoundfont.sf2 -f=wav -r=99
Input file formats: mid, rmi, kar(karaoke)
Output formats: MP3, WAV
Available options:
-i full path to input midi file
-o full path to output file
-s full path to sf2 soundfont file
-f output format (mp3, wav)
-b mp3 file bitrate (8-320)
-r reverberation (0-127)
-p cpu usage priority (0-100)
Use /?, -h or --help to get more information.