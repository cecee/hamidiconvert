import sys, getopt
import json
import glob
from shutil import copyfile
import os
import numpy as np
from shutil import copyfile

from data_base.postgreSql import *
from data_base.sqlite import *
from data_base.excell import *

def main(argv):
    FILE_NAME     = argv[0] # command line arguments의 첫번째는 파일명
    OUT_FILENAME  = ""     # 채널명 초기화
    SONG_NUMBER ="100000"
    #test()

    #calcpkg.operation.add(10, 20)
    try:
        # opts: getopt 옵션에 따라 파싱 ex) [('-i', 'myinstancce1')]
        # etc_args: getopt 옵션 이외에 입력된 일반 Argument
        # argv 첫번째(index:0)는 파일명, 두번째(index:1)부터 Arguments
        opts, etc_args = getopt.getopt(argv[1:], "hu:a", ["help","update=","auto"])
        #print(etc_args)
    except getopt.GetoptError: # 옵션지정이 올바르지 않은 경우
        print(FILE_NAME, '-u <update song number> -a <make new song number>')
        sys.exit(2)

    for opt, arg in opts: # 옵션이 파싱된 경우
        if opt in ("-h", "--help"): # HELP 요청인 경우 사용법 출력
            print(FILE_NAME, '-u <update song number> -a <auto make new song number>')
            sys.exit()

        elif opt in ("-u", "--update"): # 인스턴명 입력인 경우
            SONG_NUMBER = arg

        elif opt in ("-a", "--auto"): # 인스턴명 입력인 경우
            SONG_NUMBER = 'AUTO'
        # elif opt in ("-o", "--outfile"): # 채널명 입력인 경우
        #     OUT_FILENAME = arg

    # 필수항목 값이 비어있다면
    if len(SONG_NUMBER) < 1: # 필수항목 값이 비어있다면
        print(FILE_NAME, "-u option is mandatory") # 필수임을 출력
        sys.exit(2)

    print(SONG_NUMBER)   

    #sys.exit(2)

    ###############################################################################    
    excel = MkExcell()
    postSql = PostSQL() 
    #ha_sqlite = HaSqlite() 
    if(SONG_NUMBER == 'AUTO'):
        #sn=ha_sqlite.getSongNumber() #sqlite db에 저장된 번호 뒤부터 처리
        sn=postSql.getSongNumber() #postgre db에 저장된 번호 뒤부터 처리        
    else:
        sn = int(SONG_NUMBER)
    print(sn)
    ha_files=glob.glob(os.path.abspath('.')+"/data_s/data_hamidi/*.mid")
    #print(ha_files)

    for src_file in ha_files:
        head, tail = os.path.split(src_file)    
        tail= tail.lower()
        tail=tail.replace(".mid",".txt")
        #print(tail)  
        des_file = head+"/"+ tail
        #print(des_file)  
        if os.path.isfile(des_file):
            excel.mk_list( 0, sn, src_file, des_file) 
            sn=sn+1
        else: 
            excel.mk_list( 1, 0, src_file, "xxxx") 
        #print('{0} | {1}'.format(head, tail))

    rd_list = excel.rd_list()
    ex_list= rd_list.values
    for each_list in ex_list:
        #print('{0} | {1} | {2}'.format(each_list[0], each_list[1],each_list[2]))
        src = each_list[1]
        src= src.replace("\\","/")
        dst = "./data_s/data_midi_txt/%d.mid" % each_list[0]
        dst= dst.replace("\\","/")
        #print('src-> {0} | des->{1} '.format(src, dst))
        copyfile(src, dst) #copy midi file
        ######################################################
        head, tail = os.path.split(src)  
        print(head)  
        head= head.replace("data_hamidi","data_mp3") 
        #print(head)  
        #print('midi file des->{0} '.format(dst))
        #######################################################
        src = each_list[2]
        src= src.replace("\\","/")
        dst = "./data_s/data_midi_txt/%d.txt" % each_list[0]
        dst= dst.replace("\\","/")
        copyfile(src, dst) #copy txt file

        #######################################################
        # MIDI2MP3
        #######################################################
        SongNum = each_list[0]
        filePath = os.path.dirname(__file__)
        infile  ='{}/data_s/data_midi_txt/{}.mid'.format(filePath, SongNum) 
        copyfile(infile, 'c:/temp/tmp.mid') #copy txt file

        outfile ='{}/data_s/data_mp3/{}.mp3'.format(filePath, SongNum) 
        infile = infile.replace("\\","/")
        outfile = outfile.replace("\\","/")
        print('MIDI2MP3 IN : {}  OUT : {}\n'.format(infile, outfile) )
        #os.system('dir ./data_s/{}'.format('.'))
        cmd='c:\midiconvert\midiconverter.exe -i="{}" -o="{}" -s=c:\midiconvert\soundfont.sf2 -b=320 -r=0'.format('c:/temp/tmp.mid', 'c:/temp/tmp.mp3')
        #print(cmd)
        os.system(cmd)
        copyfile('c:/temp/tmp.mp3', outfile) #copy txt file
       
    ########################################################
    #make W file
    ########################################################

    # db_jdoc={"genre": 66, "title": "신곡테스트", "singer": "", "songno": "10", "writer": "", "keycode": "Ab", "codepage": 1200, "composer": "", "keycoden": 66, "songdate": "2006-03-02T00:00:00", "subtitle": "", "keygender": "a", "countrycode": 410}

    # fetch_data = ha_sqlite.fetchAll()
    # for i in fetch_data:
    #     #print(i)
    #     db_jdoc["songno"]=i[0]
    #     db_jdoc["genre"]=i[1]
    #     db_jdoc["title"]=i[2]
    #     db_jdoc["subtitle"]=i[3]
    #     db_jdoc["keycode"]=i[4]
    #     db_jdoc["composer"]=i[5]
    #     db_jdoc["writer"]=i[6]
    #     db_jdoc["singer"]=i[7]
    #     db_jdoc["keycoden"]=i[8]
    #     db_jdoc["songdate"]=i[9]
    #     db_jdoc["countrycode"]=i[10]
    #     db_jdoc["codepage"]=i[11]
    #     db_jdoc["keygender"]=i[12] #m,w , a
    #     print(db_jdoc)
    #     postSql.updatePostSQL(i[0], db_jdoc)
if __name__ == "__main__":
    main(sys.argv)